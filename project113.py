#!/usr/bin/python

from random import sample
import unittest

# All this information is taken from here https://en.wikipedia.org/wiki/113_(number)
S = """113 is the natural number following 112 and preceding 114.
113 is the 30th prime number (following 109 and preceding 127), so it can only be divided by one and itself. 
113 is a Sophie Germain prime, a Chen prime and a Proth prime as it is a prime number of the form 7 × 2^4 + 1. 
113 is an Eisenstein prime with no imaginary part and real part of the form 3n-1. 
113 in base 10, this prime is a primeval number, and a permutable prime with 131 and 311.
113 is a highly cototient number and a centered square number.
355/113 approximates pi to six decimal places, with an error of less than 1/1133. This means that 113/355 approximates the reciprocal of pi.
113, the number of surah al-Falaq in the Qur'an
Psalms 113–118 constitute the Hallel, which is recited on the three great feasts: Passover, Weeks, and Tabernacles
113, the atomic number of the element Nihonium
Cadmium-113m, a radioisotope and nuclear isomer with a halflife of 14.1 years
113, The fire emergency telephone number in Indonesia
113, The intelligence agency telephone number in Iran
113, The medical emergency telephone number in Latvia and Norway
113, The police emergency telephone number in Italy, Luxembourg, Slovenia, and Vietnam
113, The time telephone number in Argentina
113, The mental health crisis hotline telephone number in the Netherlands
113 (band), a French hip hop group
113, the port number of the IDENT Internet protocol that helps identify the user of a particular TCP connection
113, in enduro, is regarded as an unlucky number to be given to a race entrant and is colloquially known as a 'blind pew'"""

def get_info_on_113():
    return sample(S.split("\n"), 1)[0]

class Test113Methods(unittest.TestCase):

    def test_assert_no_empty_strings(self):
        self.assertTrue(min([len(str) for str in S.split("\n")]) > 0)

    def test_all_strings(self):
        self.assertTrue(all([type(str) == type("string") for str in S.split("\n")]))

    def test_113_mentioned(self):
        self.assertTrue(all(["113" in str for str in S.split("\n")]))

    def test_something_returned(self):
        self.assertTrue(get_info_on_113() in S)

if __name__ == '__main__':
    print(get_info_on_113())
