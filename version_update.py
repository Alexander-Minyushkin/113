#!/usr/bin/python

import argparse
import string
import unittest
import sys

version_chars = string.ascii_lowercase

def build_to_num(bld, chars = version_chars):
    res = 0
    for c in bld:
        res = res*len(chars) + chars.index(c)
    return(res)

def num_to_build(num, bld_len = 3, chars = version_chars):
    res = ''
    n=num
    while n>0:
        res = chars[n % len(chars)] + res
        n = n // len(chars)    
    
    output = chars[0]*bld_len + res
    return( output[len(res):] )

def change_build(bld, change = 1, chars = version_chars):
    return (num_to_build( build_to_num(bld, chars) + change, len(bld), chars))

class TestVersionUpdateMethods(unittest.TestCase):

    def test_assert(self):
        self.assertTrue(True)

    def test_build_to_num(self):
        chars = 'abcdefghij'
        self.assertTrue(build_to_num('aaa', chars)==0)

    def test_build_to_build(self):
        chars = 'abcdefghij'
        test_build = 'bca'
        self.assertTrue( test_build == num_to_build(build_to_num(test_build, 
                                                                chars=chars), 
                                                    chars=chars))

    def test_num_to_num(self):
        chars = 'abcdefghij'
        test_num = 113
        self.assertTrue( test_num == build_to_num(num_to_build(test_num, 
                                                                chars=chars), 
                                                    chars=chars))

    def test_change(self):
        chars = 'abcdefghij'
        test_build = 'bca'
        change = 4
        self.assertTrue(  build_to_num( change_build(test_build, change, chars=chars), 
                                        chars=chars) == 
                            build_to_num( test_build, chars=chars) + change)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Update version')
    parser.add_argument('-v', default='', required=False)
    args = parser.parse_args()

    bld = ''
    if len(args.v) > 0:
        print(change_build(args.v))
    else:        
        for line in sys.stdin:
            print(num_to_build(int(line.rstrip())))

        
