#!/usr/bin/python

import sys
from PIL import Image, ImageDraw
 
# based on code from here https://code-maven.com/create-images-with-python-pil-pillow
if __name__ == '__main__':    
    for line in sys.stdin:
        img = Image.new('RGB', (50, 20), color = (200, 200, 200))
        d = ImageDraw.Draw(img)
        d.text((10,5), line, fill=(50,50,50))
        img.save('build.png')